<!--- if(has(addTabBtn) && addTabBtn == 'true' && permission(page+':add')){ --->
<a class="btn btn-success" onclick="$.operate.addTab()">
    <i class="fa fa-plus"></i> 新增
</a>
<!--- } --->

<!--- if(has(addBtn) && addBtn == 'true' && permission(page+':add')){ --->
<a class="btn btn-success" onclick="${has(addClick)?addClick:'$.operate.add()'}">
    <i class="fa fa-plus"></i> 新增
</a>
<!--- } --->

<!--- if(has(editTabBtn) && editTabBtn == 'true' && permission(page+':edit')){ --->
<a class="btn btn-primary single disabled" onclick="$.operate.editTab()">
    <i class="fa fa-edit"></i> 修改
</a>
<!--- } --->

<!--- if(has(editBtn) && editBtn == 'true' && permission(page+':edit')){ --->
<a class="btn btn-primary single disabled" onclick="$.operate.edit()">
    <i class="fa fa-edit"></i> 修改
</a>
<!--- } --->

<!--- if(has(removeBtn) && removeBtn == 'true' && permission(page+':remove')){ --->
<a class="btn btn-danger multiple disabled" onclick="$.operate.removeAll()">
    <i class="fa fa-remove"></i> 删除
</a>
    <!--- if(has(cleanBtn) && cleanBtn == 'true'){ --->
    <a class="btn btn-danger" onclick="$.operate.clean()">
        <i class="fa fa-trash"></i> 清空
    </a>
    <!--- } --->
<!--- } --->
<!--- if(has(importBtn) && importBtn == 'true' && permission(page+':import')){ --->
<a class="btn btn-info" onclick="$.table.importExcel()" >
    <i class="fa fa-upload"></i> 导入
</a>
<!--- } --->
<!--- if(has(exportBtn) && exportBtn == 'true' && permission(page+':export')){ --->
<a class="btn btn-warning" onclick="$.table.exportExcel()" >
    <i class="fa fa-download"></i> 导出
</a>
<!--- } --->
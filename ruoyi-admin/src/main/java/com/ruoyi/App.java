package com.ruoyi;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * 启动程序
 *
 * @author ruoyi
 */
//@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
@SpringBootApplication
@MapperScan({"com.ruoyi.biz.mapper","com.ruoyi.system.mapper"})
public class App
{
    public static void main(String[] args)
    {
        // System.setProperty("spring.devtools.restart.enabled", "false");
        SpringApplication.run(App.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  RuoYi 启动成功   ლ(´ڡ`ლ)ﾞ  \n");
    }
}

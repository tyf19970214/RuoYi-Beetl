package com.ruoyi.biz.controller.api;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.bo.ResponseData;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.SimpleMap;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.service.ISysUserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 项目列表Controller
 *
 * @author LiuQi
 * @date 2019-10-05
 */
@Controller
@RequestMapping("/api/select")
public class ApiSelectController extends BaseController
{

    @Autowired
    private ISysUserService userService;


    @PostMapping("/user")
    @ResponseBody
    public ResponseData user()
    {
        List<SysUser> list = userService.selectUserList(new SysUser());
        List<SimpleMap> data = new ArrayList<>();
        for (SysUser info : list) {
            if(info.getUserId().intValue() == 1){
                continue;
            }
            data.add(new SimpleMap().set("name",info.getUserName()).set("value",info.getUserId()));
        }
        return SUCCESS(data);
    }

}
